# Build and serve
FROM node:latest AS builder

RUN git clone https://github.com/vuetifyjs/vuetify.git /vuetify

WORKDIR /vuetify

RUN yarn && yarn build

EXPOSE 8095
CMD [ "yarn", "start" ]